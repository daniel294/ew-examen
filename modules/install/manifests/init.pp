class install {
	
	stage{'yumreposd':}

	Stage['yumreposd'] -> Stage['main']
	
	class {'yum':
	    extrarepo => ['epel','remi','remi_php54'],
	    stage => 'yumreposd'
	}
	
	class { "apache": }
	
	apache::vhost { 'myMpwar.dev':
	      port          => '80',
	      docroot       => '/var/www/development/',
	      docroot_owner => 'vagrant',
	      docroot_group => 'vagrant',
	}
	
	apache::vhost { 'myMpwar.pro':
	      port          => '80',
	      docroot       => '/var/www/production/',
	      docroot_owner => 'vagrant',
	      docroot_group => 'vagrant',
	}
	
	file {'/var/www/development':
		ensure 		=> 'directory'
	}
	
	file {'/var/www/production':
		ensure 		=> 'directory'
	}
	
	file {'/var/www/development/info.php':
		ensure 		=> 'present',
		content => '<?php phpinfo(); '
	}
	
	file {'/var/www/development/index.php':
		ensure 		=> 'present',
		content => '<?php echo "<h1>Hello World</h1>  S.O: ".php_uname("s")." - ". php_uname("r")."Version: ".php_uname("v"); '
	}

	class { 'php': }
	
	php::augeas {
	
	  'php-memorylimit':
	    entry => 'PHP/memory_limit',
	    value => '256M';
	
	  'php-displayerrors':
	    entry => 'PHP/display_errors',
	    value => 'On';
	
	  'php-errorreporting':
	    entry => 'PHP/error_reporting',
	    value => 'E_ALL & ~E_DEPRECATED & ~E_STRICT';
	
	  'php-htmlerrors':
	    entry => 'PHP/html_errors',
	    value => 'On';
	
	  'php-datetimezone':
	    entry => 'Date/date.timezone',
	    value => 'Europe/Madrid';
	}
	
	php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap', 'mysqlnd']: }
	
	
	$databases = {
	  'mympwar_exam' => {
	    ensure  => 'present',
	    charset => 'utf8',
	    collate => 'utf8_general_ci'
	  },
	  'mympwar_backup' => {
	    ensure  => 'present',
	    charset => 'utf8',
	    collate => 'utf8_general_ci'
	  },
	}
	
	class { '::mysql::server':
	  root_password => 'root',
	  databases     => $databases,
	}
	
	class { 'timezone':
		timezone => 'Europe/Madrid',
	}
	
	class {'iptables':}

}
